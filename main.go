package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"
)

const (
	userIDcontext = "userID"
)

func main() {
	http.HandleFunc("/", handle)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func handle(w http.ResponseWriter, r *http.Request) {
	id := r.Header.Get(userIDcontext)
	ctx := context.WithValue(r.Context(), userIDcontext, id)
	result := processLongTask(ctx)
	w.Write([]byte(result))
}

func processLongTask(ctx context.Context) string {
	id := ctx.Value(userIDcontext)

	select {
	case <-time.After(2 * time.Second):
		return fmt.Sprintf("regular exit, id=%s\n", id)
	case <-ctx.Done():
		log.Printf("emergency exit, id=%s\n", id)
		return ""
	}
}
